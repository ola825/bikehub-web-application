import React from 'react';

class Content extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);

    fetch('/api/form-submit-url', {
      method: 'POST',
      body: data,
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label> Enter username</label>
        <input id="username" name="username" type="text" />

        <br />

        <label> Enter your email</label>
        <input id="email" name="email" type="email" />

        <br />

        <label htmlFor="birthdate">Enter your birth date</label>
        <input id="birthdate" name="birthdate" type="text" />

        <br />

        <button>Submit</button>
      </form>
    );
  }
}

export default Content;
