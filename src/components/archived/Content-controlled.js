import React from 'react';

class NewBikeForm extends React.Component {
  constructor(props) {
    super();

    this.state = {
      frameNumber: '',
      type: '',
      keyNumber: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleFrameNoChange = (event) => {
    this.setState({ frameNumber: event.target.value });
  };

  handleKeyNoChange = (event) => {
    this.setState({ keyNumber: event.target.value });
  };

  handleTypeChange(event) {
    this.setState({ type: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();

    console.log("Frame No: " + this.state.frameNumber);
    console.log("Key Number: " + this.state.keyNumber);
    console.log("Type: " + this.state.type);


    // fetch('/api/form-submit-url', {
    //   method: 'POST',
    //   // body: data,
    // });
  }

  render() {
    return (
      <div>
      <h1>Add New Bike</h1>
      <form onSubmit={this.handleSubmit}>
        <label> Frame Number</label>
        <input id="frameNumber" value={this.state.frameNumber} type="text" onChange={this.handleFrameNoChange}/>

        <br />

        <label> Type</label>
        <div onChange={this.handleTypeChange.bind(this)}>
          <input type="radio" value="Ladies" name="gender" /> Ladies
          <input type="radio" value="Gents" name="gender"/> Gents
        </div>

        <label> Key Number</label>
        <input id="keyNumber" value={this.state.keyNumber} type="text" onChange={this.handleKeyNoChange} />

        <br />
        <button>Submit</button>
      </form>
      </div>
    );
  }
}

export default NewBikeForm;
