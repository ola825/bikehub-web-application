import React from 'react';
import axios from 'axios';
import moment from 'moment'
import history from '../components/History.js';

import {FormControl, Form, FormGroup, ControlLabel, Checkbox, Button, Grid, Row, Col, PageHeader} from 'react-bootstrap';

import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import { DateRangePicker } from 'react-dates';
import ReactDOM from 'react-dom'


class HireAgreement extends React.Component {
 constructor(props) {
   super();
   

   this.state = {
      data:[],
      bicycle: null,
      fullName: null,
      startDate: null,
      endDate: null,
      address: null,
      mobileNumber: null,
      email: null,
      studentNumber: null,
      focusedInput: null,
   };

   this.handleSubmit = this.handleSubmit.bind(this);
   this.handleCancel = this.handleCancel.bind(this);

 }

 componentDidMount() {
  
  axios.get('http://localhost:3001/available')
    .then(response => {

      this.setState({
        data: response.data.data,
      });
      console.log(this.state.data.length + " Available Bikes sucessfully pulled from API", this.state.data)

    })
    .catch(error => {
      console.log('Error fetching bikes from API', error);
    });

}
 
 handleChange = event => {
  this.setState({
    [event.target.id]: event.target.value
  });
}

handleCancel = event => {
  
  history.push('/');
  window.location.reload();

}

validateForm() {
  return  this.state.fullName != null && this.state.startDate != 'null' && this.state.endDate != null & this.state.address != null && this.state.mobileNumber != null && this.state.email != null && this.state.studentNumber != null;
}


 handleSubmit(event) {
   event.preventDefault();

   axios.post('/hire', {
     frameNumber: ReactDOM.findDOMNode(this.select).value,
     startDate:  moment(this.state.startDate).format('YYYY/MM/DD'),
     endDate:  moment(this.state.endDate).format('YYYY/MM/DD'),
     fullName: this.state.fullName,
     address: this.state.address,
     mobileNumber: this.state.mobileNumber,
     email: this.state.email,
     studentNumber : this.state.studentNumber
  })
  .then(function (response) {
    console.log("Hire Successful ");
    
    history.push('/hire-info');
    window.location.reload();
      
  })
  .catch(function (error) {
    console.log(error);
  });
  
 }
 render() {
  
   return (

      <div>
    
<Grid>
  <Row className="show-grid">
  <PageHeader>Hire Agreement Form </PageHeader>
    <Col xs={12} md={8}>
        
        
        <FormGroup controlId="formControlsSelect" bsSize="large">
            <ControlLabel>Choose a Bicycle</ControlLabel>
            <FormControl ref={select => { this.select = select }} componentClass="select" placeholder="select"> 
            {/* <option value="other">{rows}</option> */}
{
        this.state.data.map(function(bike) {
          return <option key={bike.Frame_Number}
            value={bike.Frame_Number}>{bike.Frame_Number}</option>;
        })
      }
          </FormControl>
        </FormGroup>
      
    
   <ControlLabel>Hire Period</ControlLabel> 
          <FormGroup>
            <DateRangePicker
              displayFormat={'DD/MM/YYYY'}
              startDate={this.state.startDate}
              endDate={this.state.endDate}
              onDatesChange={({ startDate, endDate }) => { this.setState({ startDate, endDate })}}
              focusedInput={this.state.focusedInput}
              onFocusChange={(focusedInput) => { this.setState({ focusedInput })}}
            />
          </FormGroup>
         
    <FormGroup controlId="fullName" bsSize="large">
      <ControlLabel>Full Name</ControlLabel>
        <FormControl
          autoFocus
          type="name"
          value={this.state.fullName}
          onChange={this.handleChange}
        />
    </FormGroup>
        
        <FormGroup controlId="address" bsSize="large">
          <ControlLabel>Address</ControlLabel>
          <FormControl
            value={this.state.address}
            onChange={this.handleChange}
            type="address"
          />
        </FormGroup>
        
        <FormGroup controlId="mobileNumber" bsSize="large">
          <ControlLabel>Mobile Number</ControlLabel>
          <FormControl
            value={this.state.mobileNumber}
            onChange={this.handleChange}
            type="mobile-number"
          />
        </FormGroup>

        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>E-mail Address</ControlLabel>
          <FormControl
            value={this.state.email}
            onChange={this.handleChange}
            type="email"
          />
        </FormGroup>

        <FormGroup controlId="studentNumber" bsSize="large">
          <ControlLabel>Student Number</ControlLabel>
          <FormControl
            value={this.state.studentNumber}
            onChange={this.handleChange}
            type="text"
          />
        </FormGroup>

      
   
      <Button bsStyle='success' type='submit' onClick ={this.handleSubmit}  disabled={!this.validateForm()} >Submit</Button>
      <Button bsStyle='danger' onClick={this.handleCancel}>Cancel</Button>
   
      
    </Col>
  </Row>
  </Grid>  

  
   
    

      </div>

   );
 }
}

export default HireAgreement;
