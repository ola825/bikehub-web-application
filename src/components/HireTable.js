//component that renders list of bikes
import React from 'react';
import axios from 'axios';
import moment from 'moment';
import history from '../components/History.js';
import {withRouter} from 'react-router-dom';


class HireTable extends React.Component{

  constructor(){
      super();
        this.state = {
            data:[]
      }
  }

  componentDidMount() {

  this.getHires();

  }

  getHires() {

    axios.get('http://localhost:3001/hires')
    .then(response => {

      this.setState({
        data: response.data.data,
      });
      console.log(this.state.data.length + " Hire information sucessfully pulled from API", this.state.data)


    })
    .catch(error => {
      console.log('Error fetching hire information from API', error);
    });

  }



  render() {
    const rows = this.state.data.map((bike) =>
    {
       return <HireTableRow key={bike.Frame_Number} data={bike}/>
    });

         return (
           <div>
           <h1>Hires</h1>
              <table className="table table-bordered table-hover table-responsive"  style={{width:"100%"}}>
                  <HireTableHead/>
                    <tbody>{rows}</tbody>
                </table>
            </div>
             );
     }
}


class HireTableHead extends React.Component{

  render() {
         return (
        <thead>
          <tr>
            <th></th>
            <th>Bicycle</th>
            <th>Student Number</th>
            <th>Name</th>
            <th>Return Date</th>
            <th>Action</th>
          </tr>
        </thead>
        );
  }
}

class HireTableRow extends React.Component{

    determineColourIndicator(endDate){
        
        endDate = moment(endDate, 'YYYY-MM-DD').toDate();
    
        var now = moment().toDate();

        if (now > endDate) {

            // console.log("Date is past... bike is overdue");
            return(<div style={{ color: "#ff0000" }}>◉</div>);
        }
        else{
            // console.log("bike is not due");
            return(<div style={{ color: "#00FF7F" }}>◉</div>);
        }
     
      
    }

    setActionButtons(endDate){

        endDate = moment(endDate, 'YYYY-MM-DD').toDate();
    
        var now = moment().toDate();

        if (now > endDate) {

            console.log("Date is past... bike is overdue");
              return(  
                <div>
                    <button className='btn btn-danger' style={{margin:"5px"}}>
                        <span className="glyphicon glyphicon-flag"></span> Flag
                    </button>

                    <button className='btn btn-primary' onClick={this.returnBicycle.bind(null,this.props.data.Student_Number, this.props.data.Frame_Number)} >
                        <span className="glyphicon glyphicon-ok"></span> Return
                    </button>
                </div>
              );
        }
        else{
            console.log("bike is not due");
            return(
                <div>
                    <button className='btn btn-success' style={{margin:"5px"}} onClick={this.returnBicycle.bind(null,this.props.data.Student_Number, this.props.data.Frame_Number)}>
                        <span className="glyphicon glyphicon-ok"></span> Return
                    </button>

                    <button className='btn btn-warning' >
                        <span className="glyphicon glyphicon-pencil"></span> Edit
                    </button>
                </div>
            );
        }

        

    }

  render(){

    // console.log(this.props.data[0]);
    return (

      <tr>
        <td> {this.determineColourIndicator(this.props.data.End_Date)} </td>
        <td>{this.props.data.Frame_Number}</td>
        <td>{this.props.data.Student_Number}</td>
        <td>{this.props.data.Name}</td>
        <td> {moment(this.props.data.End_Date).format('DD/MM/YYYY')}</td>
        <td>
        
        {this.setActionButtons(this.props.data.End_Date)}
          {/* <button className='btn btn-danger' onClick={this.deleteBike.bind(null,this.props.data.Frame_Number)}>
            <span className="glyphicon glyphicon-trash"></span> Delete
          </button> */}
        </td>

      </tr>
    );
   }

   returnBicycle = (studentNumber, frameNumber) =>{

    console.log("Returning Bicycle " + frameNumber + " that was hired to " + studentNumber);
  
    axios.post('/hire/return', {
      studentNumber : studentNumber,
      frameNumber: frameNumber
     
   })
   .then(function (response) {
     console.log("Return Successful ");
     
     history.push('/hire-info');
     window.location.reload();
       
   })
   .catch(function (error) {
     console.log(error);
   });
 
    
  }
}

export default withRouter(HireTable);
