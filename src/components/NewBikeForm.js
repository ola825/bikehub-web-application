import React from 'react';
import axios from 'axios';
import {FormControl, Form, FormGroup, ControlLabel, Checkbox,  Button, Grid, Row, Col, PageHeader} from 'react-bootstrap';
import history from '../components/History.js';

class NewBikeForm extends React.Component {
  constructor(props) {
    super();

    this.state = {
      pageTitle: '',
      frameNumber: '',
      type: '',
      keyNumber: '',
      validationState1: null,
      validationState2: null,
      data: []
      
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    const { frameNumber2 } = this.props.match.params;
    console.log(frameNumber2)

    this.getBike();
  }

  handleChange = event => {
    
    this.setState({
      [event.target.id]: event.target.value,
    });
 
  }

  cancel(){
    history.push('/');
    window.location.reload();
  }

  handleTypeChange(event) {
    this.setState({ type: event.target.value });
  }
 
  
  validateForm() {
    
    return this.state.keyNumber.length > 0 && this.state.frameNumber.length > 0 && this.state.type != '';
  }

  getBike(){

    const { frameNumber } = this.props.match.params;

    if(frameNumber == null || frameNumber == ''){
      console.log("no bike provided")
      this.setState({pageTitle: 'Add Bike'});
      return;
    }
    else{
      console.log("Getting Bike " + frameNumber + " from API...");
    }
    
    axios.get('http://localhost:3001/bike/' + frameNumber)
    .then(response => {

      this.setState({
        data: response.data.data,
        frameNumber: response.data.data.Frame_Number,
        keyNumber: response.data.data.Key_Number,
        type: response.data.data.Type,
        pageTitle: 'Edit Bike ' + frameNumber
        
      });
      console.log("Bicycle sucessfully pulled from API", this.state.data);


    })
    .catch(error => {
      console.log('Error fetching bicycle from API', error);
    });
  }

  


  handleSubmit(event) {
    event.preventDefault();

    console.log("Frame No: " + this.state.frameNumber);
    console.log("Key Number: " + this.state.keyNumber);
    console.log("Type: " + this.state.type);

    axios.post('/bike', {
      frameNumber: this.state.frameNumber,
      type: this.state.type,
      keyNumber: this.state.keyNumber
    })

    this.props.history.push('/');
    window.location.reload();
      

  }

  render() {
    return (
      <div>
      <PageHeader>{this.state.pageTitle}</PageHeader>
    
  <Grid>
  <Row className="show-grid">
    <Col xs={6} md={4}>
        
    <FormGroup controlId="frameNumber" bsSize="large" validationState={this.state.validationState1} >
          <ControlLabel>Frame Number</ControlLabel>
            <FormControl
              autoFocus
              type="text"
              value={this.state.frameNumber}
              onChange={this.handleChange}
              disabled=""
            />
        </FormGroup>


    <FormGroup controlId="keyNumber" bsSize="large" validationState={this.state.validationState2}>
      <ControlLabel>Key Number</ControlLabel>
        <FormControl
          autoFocus
          type="text"
          value={this.state.keyNumber}
          onChange={this.handleChange}
        />
    </FormGroup>

    <FormGroup>
      <ControlLabel>Type</ControlLabel>
        <div onChange={this.handleTypeChange.bind(this)}>
          <input type="radio" value="Ladies" name="type" /> Ladies <span></span>

          <input type="radio" value="Gents" name="type"/> Gents
        </div>
    </FormGroup>

    <Button bsStyle='success' type='submit' bsSize="large" disabled={!this.validateForm()} onClick={this.handleSubmit}>Submit </Button>
    <Button bsStyle="danger" onClick={this.cancel} bsSize="large" >Cancel</Button>
    </Col>
  </Row>
  </Grid>
      </div>
    );
  }
}

export default NewBikeForm;
