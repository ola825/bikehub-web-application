//component that renders list of bikes
import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';
import history from '../components/History.js';


class BikeTable extends React.Component{

  constructor(){
      super();
        this.state = {
            data:[],
      }
      this.getBicycles = this.getBicycles.bind(this);
  }

  componentDidMount() {
      this.getBicycles();
  }

  getBicycles(){

    axios.get('http://localhost:3001/bikes')
    .then(response => {

      this.setState({
        data: response.data.data,
      });
      console.log(this.state.data.length + " Bikes sucessfully pulled from API", this.state.data)


    })
    .catch(error => {
      console.log('Error fetching bikes from API', error);
    });
  }

  render() {
    const rows = this.state.data.map((bike) =>
    {
    
       return <BikeTableRow key={bike.Frame_Number} data={bike}/>
    });

         return (
           <div>
           <h1>Bikes</h1>
              <table className="table table-bordered table-hover table-responsive"  style={{width:"100%"}}>
                  <BikeTableHead/>
                    <tbody>{rows}</tbody>
                </table>
            </div>
             );
     }
}
class BikeTableHead extends React.Component{

  render() {
         return (
        <thead>
          <tr>
            <th>Frame No.</th>
            <th>Type</th>
            <th>Key No.</th>
            <th>Action</th>
          </tr>
        </thead>
        );
  }
}

class BikeTableRow extends React.Component{

  render(){
    
    return (
  
      <tr>
        <td>{this.props.data.Frame_Number}</td>
        <td>{this.props.data.Type}</td>
        <td>{this.props.data.Key_Number}</td>
        <td>
       
        <button className='btn btn-warning btn-edit-robot' onClick={this.editBike.bind(null,this.props.data.Frame_Number)} style={{margin:"5px"}}>
          <span className="glyphicon glyphicon-pencil"></span> Edit
        </button>

          <button className='btn btn-danger' onClick={this.deleteBike.bind(null,this.props.data.Frame_Number)}>
            <span className="glyphicon glyphicon-trash"></span> Delete
          </button>
        </td>
      </tr>
    );
   }

  
  editBike = (frameNumber) => {
    history.push('/bike/' + frameNumber);
    window.location.reload();
  }
   
  deleteBike = (Frame_Number) => {
    
     console.log("Deleting Bike: " + Frame_Number);
     axios.delete('/bike/'+ Frame_Number)
     .then(response => {
       console.log("Bike successfully deleted");
    
       history.push('/');
       window.location.reload();
       
  })
     .catch(error => {
       console.log('Error deleting bicycle', error);
     });

   }
}


export default withRouter(BikeTable);
