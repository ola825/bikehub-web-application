import React, { Component } from 'react';
import './light-bootstrap-dashboard.css';
import BikeTable from './components/BikeTable.js';
import NewBikeForm from './components/NewBikeForm.js';
import HireAgreement from './components/HireAgreement.js';
import HireTable from './components/HireTable.js';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {Link} from 'react-router-dom';
import ScrollArea from 'react-scrollbar';
import history from './components/History.js';


class App extends Component {
  render() {
    return (
      <Router history={history}>
      <div className="wrapper">
        <div className="sidebar" data-color="blue" >
        
            <div className="sidebar-wrapper">
                <div className="logo"> 
                        <h3> Dashboard </h3>
                </div>
                <ul className="nav">
                    <li className="nav-link">
                        <Link to='/add-bike' className="nav-link"> 
                            <p>Add New Bike</p>
                        </Link> 
                    </li>
                    <li className="nav-link">
                        <Link to='/' className="nav-link"> 
                            <p>Bike Inventory</p>
                        </Link> 
                    </li>
                    <li className="nav-link">
                        <Link to='/new-hire' className="nav-link"> 
                            <p>Hire Agreement Form</p>
                        </Link> 
                    </li>
                    <li className="nav-link">
                        <Link to='/hire-info' className="nav-link"> 
                            <p>Hire Information</p>
                        </Link> 
                    </li>
                  
                    <li className="nav-item active active-pro">
                        <a className="nav-link active" href="upgrade.html">
                            <i className="nc-icon nc-lock-circle-open"></i>
                            <p>Log Out</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div className="main-panel">
           
            <div className="content">
                <div className="container-fluid">
                  <div className="row">
                    
                    <div className="col-4">
                    
                      <Route exact path='/' component={BikeTable} />
                      <Route path="/add-bike" component={NewBikeForm}/>
                      <Route path='/bike/:frameNumber' component={NewBikeForm} />
                      <Route path='/new-hire' component={HireAgreement} /> 
                      <Route path='/hire-info' component={HireTable} /> 

                    </div>

                  </div>
                </div>        
                          
            </div>
            
        </div>
    </div>
      </Router>
    );
  }
}

export default App;
