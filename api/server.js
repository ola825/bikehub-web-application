
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');

const env = require('./env');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


app.set("port", process.env.PORT || 3001);

// connection configurations
const sql = mysql.createConnection({
    host: env.DATABASE_HOST,
    user: env.DATABASE_USERNAME,
    password: env.DATABASE_PASSWORD,
    database: env.DATABASE_NAME,
    multipleStatements: true
});

// connect to database
sql.connect();


// default route
app.get('/', function (req, res) {

        let apikey = req.headers.apikey;
        let isAuthenticated = false;
        
        if(apikey == env.API_KEY){
             isAuthenticated == true;
             return res.send({ error: true, message: 'Welcome to the BikeHub Web API' })

        }
        else{
            return res.send({ error: true, message: 'UNAUTHORISED! Please provide API Key...' })
        }
    
});

//test
app.get('/test', function (req, res) {
    sql.query('SELECT * FROM sys.test', function (error, results, fields) {
        if (error) return  error;
        return res.send({ error: false, data: results});
    });
});

//retrieve all bikes
app.get('/bikes', function (req, res) {

    sql.query('SELECT * FROM Bicycle', function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, data: results});
    });
});

//get list of currently hired bicycles
app.get('/hires', function (req, res) {
    sql.query('SELECT ID, Frame_Number, Student.Name, Student.Student_Number, End_Date  FROM Hire INNER JOIN Student ON Hire.Student_Number = Student.Student_Number', function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, data: results});
    });
});

//get information from a single hire
app.get('/hire/:studentNumber', function (req, res) {

    let studentNumber = req.params.studentNumber;

    sql.query('SELECT  Frame_Number, Student.Name, Student.Student_Number, Student.Address, Student.Email, Start_Date, End_Date  FROM Hire INNER JOIN Student ON Hire.Student_Number = ?;',  studentNumber, function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, data: results});
    });
});

//retrieve all bikes
app.get('/available', function (req, res) {
    
    sql.query('SELECT Frame_Number FROM Bicycle WHERE Available = 1', function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, data: results});
    });
});

//add new bike
app.post('/bike', function (req, res) {

var frameNumber = req.body.frameNumber;
var type = req.body.type;
var keyNumber  = req.body.keyNumber;

if (!req.body){
    return res.status(400).send({ error:true, message: 'Please provide bike' });
}

    sql.query('INSERT INTO Bicycle SET ?', { Frame_Number: frameNumber, Type: type, Key_Number: keyNumber, Available: 1}, function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, data: results});
    });
});

//update bike
app.put('/bike/update/:frameNumber', function (req, res) {

    var urlFrameNumber = req.params.frameNumber
    var frameNumber = req.body.frameNumber;
    var type = req.body.type;
    var keyNumber  = req.body.keyNumber;
    
    if (!req.body){
        return res.status(400).send({ error:true, message: 'Please provide bike' });
    }
    
        sql.query('UPDATE  Bicycle SET ? WHERE Frame_Number =' + frameNumber, { Frame_Number: frameNumber, Type: type, Key_Number: keyNumber}, function (error, results, fields) {
            if (error) throw error;
            return res.send({ error: false, data: results});
        });
});


//add new hire
app.post('/hire', function (req, res) {

    var frameNumber = req.body.frameNumber;
    var startDate = req.body.startDate;
    var endDate = req.body.endDate;
    var fullName = req.body.fullName;
    var address = req.body.address;
    var mobileNumber = req.body.mobileNumber;
    var email = req.body.email;
    var studentNumber = req.body.studentNumber;

    if (!req.body){
        return res.status(400).send({ error:true, message: 'Please provide hire' });
    }

    sql.query('REPLACE INTO Student SET ?', { Student_Number: studentNumber, Name: fullName, Address: address, Email: email}, function (error, results, fields) {
        if (error) return error;
        
        
    });

    sql.query('INSERT INTO Hire SET ?', { Frame_Number: frameNumber, Student_Number: studentNumber, Start_Date: startDate, End_Date: endDate}, function (error, results, fields) {
        if (error) return error;
    });

    sql.query('UPDATE Bicycle SET Available = 0 WHERE Frame_Number = ?', frameNumber, function (error, results, fields) {
        if (error) return error;
        return res.send({ error: false, message: "Hire Successful" });
    });
});

//return bicycle
app.post('/hire/return', function (req, res) {

    var studentNumber = req.body.studentNumber;
    var frameNumber = req.body.frameNumber;
    
    if (!req.body){
        return res.status(400).send({ error:true, message: 'Please provide details' });
    }
 
        sql.query('INSERT INTO Hire_Log select * from Hire WHERE Student_Number = ? ;', studentNumber, function (error, results, fields) {
            if (error) return error;
        });

        sql.query("DELETE FROM Hire WHERE Student_Number = ? ;",  studentNumber, function (error, results, fields) {
            if (error) return error;
        });

        sql.query("UPDATE Bicycle SET Available = 1 WHERE Frame_Number = ?;", frameNumber, function (error, results, fields) {
            if (error) return error;
            return res.send({ error: false, message: "Return Successful" });
        });
        
    });

//retrive bike with frame number
app.get('/bike/:frameNumber', function (req, res) {

   let frameNumber = req.params.frameNumber;

   if (!frameNumber) {
       return res.status(400).send({ error: true, message: 'Please provide frameNumber' });
   }

   sql.query('SELECT * FROM Bicycle where Frame_Number = ?', frameNumber, function (error, results, fields) {
       if (error) return error;
       return res.send({ error: false, data: results[0]});
   });

   
});

//delete a particular bicyle by frame number
app.delete('/bike/:frameNumber', function (req, res) {

  let frameNumber = req.params.frameNumber;

  if (!frameNumber) {
      return res.status(400).send({ error: true, message: 'Please provide frameNumber' });
  }

  sql.query('DELETE FROM Bicycle where Frame_Number = ?', frameNumber, function (error, results, fields) {
      if (error) return error;
      return res.send({ error: false, data: results[0]});
  });

});



app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});
